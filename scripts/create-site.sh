#!/bin/bash

# ---------------------------------------
#       Create WordPress Site
# ---------------------------------------

mkdir /var/www/$1
cd /var/www/$1
wp core download
wp core config --dbname=$1 --dbuser=root --dbpass=root
wp db create
sudo -u vagrant wp core install --url=$1.devbox --title=$1 --admin_user=Evalest --admin_password=root --admin_email=skakinich@evalest.com

# Create Movefile

MOVEFILE=$(cat <<EOF
local:
  vhost: "http://$1.devbox"
  wordpress_path: "/var/www/$1"

  database:
    name: "$1"
    user: "root"
    password: "root"
    host: "localhost"

staging:
  vhost: "vhost"
  wordpress_path: "path"

  database:
    name: "name"
    user: "user"
    password: "password"
    host: "host"

  exclude:
    - ".git/"
    - ".gitignore"
    - ".sass-cache/"
    - "bin/"
    - "tmp/*"
    - "Gemfile*"
    - "Movefile"
    - "wp-config.php"
    - "wp-content/*.sql"

  ssh:
    host: "host"
    user: "user"
    password: "password"
EOF
)
touch Movefile
sudo echo "${MOVEFILE}" > ./Movefile
# ---------------------------------------
#       Add Version Control
# ---------------------------------------

cd wp-content/themes
git init

# ---------------------------------------
#       Add Themes
# ---------------------------------------

mkdir Evalest-$1
cd Evalest-$1

wget https://github.com/holger1411/understrap/archive/master.zip
unzip -o master.zip
mv understrap-master/* ./
rm -rf understrap-master/
rm -rf master.zip

cd /var/www/
sudo chmod 777 -R $1/*
# add https://github.com/holger1411/understrap-child/archive/master.zip
