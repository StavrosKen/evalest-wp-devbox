#!/bin/bash

# ---------------------------------------
#       Create Vhost
# ---------------------------------------

sudo chmod 777 -R /etc/apache2/sites-available/*
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$1.conf
> /etc/apache2/sites-available/$1.conf

VHOST=$(cat <<EOF
<VirtualHost *:80>
    DocumentRoot /var/www/$1/
    ServerName www.$1.devbox
    ServerAlias $1.devbox
</VirtualHost>
EOF
)

sudo echo "${VHOST}" > /etc/apache2/sites-available/$1.conf
sudo a2ensite $1.conf
sudo service apache2 restart
