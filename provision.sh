#!/usr/bin/env bash

# ---------------------------------------
#          Virtual Machine Setup
# ---------------------------------------

# Adding multiverse sources.
cat > /etc/apt/sources.list.d/multiverse.list << EOF
deb http://archive.ubuntu.com/ubuntu trusty multiverse
deb http://archive.ubuntu.com/ubuntu trusty-updates multiverse
deb http://security.ubuntu.com/ubuntu trusty-security multiverse
EOF

# Updating packages
apt-get update

# ---------------------------------------
#          Apache Setup
# ---------------------------------------

# Installing Packages
apt-get install -y apache2

# linking Vagrant directory to Apache 2.4 public directory
ln -fs /vagrant /var/www
service apache2 reload

# ---------------------------------------
#          PHP Setup
# ---------------------------------------

# Installing packages
apt-get install -y php5 libapache2-mod-php5
service apache2 reload

# ---------------------------------------
#          MySQL Setup
# ---------------------------------------

# Setting MySQL root user password root/root
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

# Installing packages
apt-get install -y mysql-server mysql-client php5-mysql

# ---------------------------------------
#          PHPMyAdmin setup
# ---------------------------------------

# Default PHPMyAdmin Settings
debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password root'
debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'

# Install PHPMyAdmin
apt-get install -y phpmyadmin

# Make Composer available globally
ln -s /etc/phpmyadmin/apache.conf /etc/apache2/sites-enabled/phpmyadmin.conf

sudo chmod 777 -R /etc/php5/*
MEMORYLIMIT=$(cat <<EOF
upload_max_filesize = 1000M
post_max_size = 2000M
memory_limit = 3000M
file_uploads = On
max_execution_time = 180
EOF
)
echo "${MEMORYLIMIT}" > /etc/php5/cli/php.ini
echo "${MEMORYLIMIT}" > /etc/php5/fpm/php.ini
echo "${MEMORYLIMIT}" > /etc/php5/apache2/php.ini

# Restarting apache to make changes
service apache2 restart

# ---------------------------------------
#       Tools Setup.
# ---------------------------------------
# These are some extra tools that you can remove if you will not be using them 
# They are just to setup some automation to your tasks. 

sudo apt-get install -y unzip

# Adding NodeJS from Nodesource. This will Install NodeJS Version 5 and npm
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

# Installing Bower and Gulp
npm install -g bower gulp

# Installing GIT
sudo apt-get install -y git-all

# Install Composer
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

# Installing Yeoman
npm install -g yo

# Installing BrowserSync
npm install -g browser-sync

# ---------------------------------------
#          WordPress
# ---------------------------------------

# Installing WP-CLI
# https://wp-cli.org/commands/
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

# Installing Ruby
curl -sSL https://get.rvm.io | bash -s stable --ruby
/bin/bash --login
rvm install 2.0.0
rvm use 2.0.0
rvm rubygems latest
ruby --version

# Installing Wordmove
# https://github.com/welaika/wordmove
sudo chmod -R 777 /usr/local/rvm/
apt-get install -y sshpass 
gem install wordmove # may need to run vagrant provision

# ---------------------------------------
#          Workflow
# ---------------------------------------

# vagrant ssh
# cd /vagrant/scripts
# ./create-site.sh mySite
# sudo ./create-host.sh mySite
# create a vhost in host environemnt (windows: Windows/System32/drivers/etc/hosts)
# Run wordmove to move to live