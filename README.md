# Evalest-WordPress-Devbox #

Scaffold WordPress sites blazingly fast with one command. In a clean, pre-configured and optimized environment.
Just write one command, and visit your new site on the host's browser!

### Included and pre-configured: ###

* Ubuntu
* Apache2
* PHP5
* MySQL
* Phpmyadmin
* Composer
* Ruby
* Git
* NodeJS
* Bower
* Gulp
* BrowserSync
* WP-CLI
* UnderStrap Theme (Underscores & Bootstrap, Gulp)
* WordMove

### Prerequisites ###

* Latest version of VirtualBox
* Latest version of Vagrant

### Set up ###

* Clone this git repo
* Navigate with your terminal in the project's root folder
* Run `vagrant up` and ignore any possible errors
* Run `vagrant provision`
* Add `127.0.0.1 evalest-wp.devbox` in your 'hosts' file
(*For windows users the file is located in /Windows/System32/drivers/etc/hosts and for Ubuntu users the file is located in /etc/hosts*)

You can now see your folders if you visit http://evalest-wp.devbox/
and your phpmyadmin if you visit http://evalest-wp.devbox/phpmyadmin

### Create a WordPress site ###

You only need to run `./create-site mySite` and `sudo ./create-host mySite` and add `127.0.0.1 mySite.devbox` in your 'hosts' file.

Detailed instructions:
* From the root of the project's folder run 'vagrant ssh'
* Navigate to /vagrant/scripts
* Run ./create-site mySite
* Run sudo ./create-host mySite
* Add `127.0.0.1 mySite.devbox` in your 'hosts' file

That's all! Now visit mySite.devbox to see your new site!

* (*In all above commands, replace 'mySite' with your site's name*)
* (Phpmyadmin username and password for default phpmyadmin is root)
* (WordPress Username is 'Evalest', password is 'root')